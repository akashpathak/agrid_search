AgridNetflix.controller('usersController', ['$scope', '$http', function ($scope, $http) {

    $scope.actorName = '';
    $scope.onChange = false;

    $scope.myChange = function (val) {

        $scope.onChange = true;
        var char = $scope.actorName;
        var url = "http://netflixroulette.net/api/api.php?actor=" + char;

        $http.get(url)
            .success(function (data) {
                $scope.users = data;

            })
            .error(function (error) {
                $scope.errorCode = error;
            });

        $scope.orderByMe = function (val) {
            $scope.myOrderBy = val;
        }

        var limitStep = 5;
        $scope.limit = limitStep;
        $scope.incrementLimit = function () {
            $scope.limit += limitStep;
        };
        $scope.decrementLimit = function () {
            $scope.limit -= 1;
        };

    };

    $scope.clearfunction = function (event) {
        event.searchAll = null;
        $scope.actorName = '';
        $scope.onChange = false;

    }
}]);
