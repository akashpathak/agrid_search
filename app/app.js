var AgridNetflix = angular.module('AgridNetflix', ['ngRoute']);

AgridNetflix.config(function($routeProvider) {
    $routeProvider
        .when('/index', {
            controller: 'usersController',
            templateUrl: 'app/templates/usersList.html'
        })
        .otherwise({
            redirectTo: '/index'
        });
});
